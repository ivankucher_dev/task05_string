package com.epam.trainings.main;

import com.epam.trainings.controller.ViewController;
import com.epam.trainings.model.Language;
import com.epam.trainings.model.menu.Menu;
import com.epam.trainings.view.View;

public class Main {
  public static void main(String[] args) {
    View view = new View();
    Menu menu = new Menu();
    Language language = new Language(menu);
    ViewController controller = new ViewController(view, language, menu);
    view.setController(controller);
  }
}
