package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import com.epam.trainings.utils.MapUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MenuThirteenthCommand implements Command {

    private TextModel model;
    private Scanner sc;
    private char symbol;
    private Map<Word,Integer> map;

    public MenuThirteenthCommand(TextModel model) {
        this.model = model;
        sc = new Scanner(System.in);
        map = new LinkedHashMap<>();
    }

    @Override
    public String execute() {
        System.out.println("Enter letter : ");
        symbol = sc.next().charAt(0);
        List<Word> allWords = model.getAllWords();
        int count;
        for (Word word : allWords) {
            count = 0;
            for (char c : word.getWord().toLowerCase().toCharArray()) {
                if (c == symbol)
                {
                    count++;
                    }
            }
            if (count > 0) {
                map.put(word, count);
                map = MapUtil.sortByValue(map);
            }
        }

        StringBuilder sb = new StringBuilder();
        map
                .forEach((w,c) ->sb.append(appender(w,c,symbol)) );
        return sb.toString();
    }

    private String appender(Word word,int amount , char letter){
        return "\n["+word.getWord() + "] has [" + amount+"] amount of ["+letter+"]";
    }
    }

