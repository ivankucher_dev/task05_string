package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MenuSixthCommand implements Command {

  private TextModel model;

  public MenuSixthCommand(TextModel model) {
    this.model = model;
  }

  @Override
  public String execute() {
    List<Word> allWords = model.getAllWords();
    StringBuilder sb = new StringBuilder();
    allWords.sort(
        new Comparator<Word>() {
          @Override
          public int compare(Word o1, Word o2) {
            return o1.getWord().compareToIgnoreCase(o2.getWord());
          }
        });
    for (int i = 0; i < allWords.size() - 1; i++) {
      if (allWords.get(i).getWord().toLowerCase().charAt(0)
          != allWords.get(i + 1).getWord().toLowerCase().charAt(0)) {

        sb.append(allWords.get(i).getWord() + "\n\n");
      } else {
        sb.append("\n" + allWords.get(i).getWord());
      }
    }
    return sb.toString();
  }
}
