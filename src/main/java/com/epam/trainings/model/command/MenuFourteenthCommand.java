package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;

public class MenuFourteenthCommand implements Command {

  private TextModel model;

  public MenuFourteenthCommand(TextModel model) {
    this.model = model;
  }


  @Override
  public String execute() {
    String text = model.getText();

    String palindrome = text.substring(0, 1);
    for (int i = 0; i < text.length(); i++) {
      String tmp = checkForPalindrome(text, i, i);
      if (tmp.length() > palindrome.length()) {
        palindrome = tmp;
      }

      tmp = checkForPalindrome(text, i, i + 1);
      if (tmp.length() > palindrome.length()) {
        palindrome = tmp;
      }
    }

    return palindrome;

  }

    public String checkForPalindrome(String s, int begin, int end) {
        while (begin >= 0 && end <= s.length() - 1 && s.charAt(begin) == s.charAt(end)) {
            begin--;
            end++;
        }
        return s.substring(begin + 1, end);
    }
}
