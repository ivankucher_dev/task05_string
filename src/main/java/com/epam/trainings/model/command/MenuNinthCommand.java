package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import com.epam.trainings.utils.MapUtil;
import com.sun.org.apache.xerces.internal.impl.xs.SchemaNamespaceSupport;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MenuNinthCommand implements Command {

    private TextModel model;
    private Map<Word,Integer> charInWord;
    private char letter;
    private Scanner sc;

    public MenuNinthCommand(TextModel model) {
        this.model = model;
        charInWord = new LinkedHashMap<>();
        sc = new Scanner(System.in);
    }

    @Override
    public String execute() {
    System.out.println("Enter letter : ");
    letter = sc.next().charAt(0);
        List<Word> allWords = model.getAllWords();
        StringBuilder sb = new StringBuilder();
        int count;
    for (Word word : allWords) {
      count = 0;
      for (char c : word.getWord().toLowerCase().toCharArray()) {
        if (c == letter) count++;
      }
      if (count != 0) {
        charInWord.put(word, count);
        charInWord = MapUtil.sortByValue(charInWord);
      }
      }
    charInWord
            .forEach((w,c) ->sb.append(appender(w,c,letter)) );
        return sb.toString();
    }

    private String appender(Word word,int amount , char letter){
        return "\n["+word.getWord() + "] has [" + amount+"] amount of ["+letter+"]";
    }
}
