package com.epam.trainings.model.command;

import com.epam.trainings.controller.ViewController;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;
import java.util.Scanner;


public class MenuSixteenthCommand implements Command {

    private static Logger log = LogManager.getLogger(ViewController.class.getName());
    private TextModel model;
    private int length;
    private int sentenceNumber;
    private String wordToReplace;
    private Scanner sc;

    public MenuSixteenthCommand(TextModel model) {
        this.model = model;
        sc = new Scanner(System.in);
    }

    @Override
    public String execute() {
        StringBuilder sb = new StringBuilder();
        boolean flag = true;
    while (flag) {
      System.out.println("Enter length : ");
      length = sc.nextInt();
      System.out.println("Enter sentence number : ");
      sentenceNumber = sc.nextInt();
      System.out.println("Enter word : ");
      sc.nextLine();
      wordToReplace = sc.nextLine();
      if (sentenceNumber < 1 || sentenceNumber > model.getSentences().size() && length < 1) {
        log.error("Incorrect input, try again");
      }
      else flag = false;
        }
        Sentence sentence = model.getSentences().get(sentenceNumber);
        List<Word> allWordsInSentence  = sentence.getWordsInSentence();
        allWordsInSentence.stream()
                .forEach(
                        word -> {
                            if (word.getWord().length() == length) {
                                word.setWord(wordToReplace);
                            }
                            sb.append("\n" + word.getWord());
                        });
        model.getSentences().get(sentenceNumber).setWordsInSentence(allWordsInSentence);
        return sb.toString();
    }
}
