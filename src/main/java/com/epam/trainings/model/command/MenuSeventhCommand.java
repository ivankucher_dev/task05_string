package com.epam.trainings.model.command;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import com.epam.trainings.utils.MapUtil;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import static com.epam.trainings.constants.CommandsConst.*;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuSeventhCommand implements Command {

  private Map<Word, Double> vowelPercent;
  private TextModel model;

  public MenuSeventhCommand(TextModel model) {
    this.model = model;
    vowelPercent = new LinkedHashMap<>();
  }

  @Override
  public String execute() {
    int vowelCounter = 0;
    double vowelPerc = 0;
    List<Word> allWords = model.getAllWords();
    StringBuilder sb = new StringBuilder();

    for (Word word : allWords) {
      vowelCounter = 0;
      vowelPerc = 0;
      Pattern pattern = Pattern.compile(VOWEL_PATTERN_STR);
      Matcher m = pattern.matcher(word.getWord());
      while (m.find()) {
        vowelCounter++;
      }

      vowelPerc = (double) vowelCounter / word.getWord().length();
      vowelPerc = (double) Math.round(vowelPerc * 100) / 100;
      vowelPercent.put(word, vowelPerc);
    }
    vowelPercent = MapUtil.sortByValue(vowelPercent);
    vowelPercent.forEach((k, v) -> sb.append("\n" + k.getWord() + " , percent of vowel = " + v*100+"%"));
    return sb.toString();
  }
}
