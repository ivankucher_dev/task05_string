package com.epam.trainings.model.command;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;

import java.util.Comparator;
import java.util.List;
import static com.epam.trainings.constants.CommandsConst.*;

public class MenuSecondCommand implements Command {

    private TextModel model;

    public MenuSecondCommand(TextModel model) {
        this.model = model;
    }

    @Override
    public String execute() {
        StringBuilder sb = new StringBuilder();
        List<Sentence> sentencesList = model.getSentences();
        sentencesList.sort(new Comparator<Sentence>() {
            @Override
            public int compare(Sentence o1, Sentence o2) {
                return o2.getWordsInSentence().size() - o1.getWordsInSentence().size();
            }
        });
        sentencesList.forEach(e->sb.append("\n"+SENTENCE_NAME + " : "+ e));
        return sb.toString();
    }
}
