package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static com.epam.trainings.constants.CommandsConst.*;

public class MenuEightCommand implements Command {
    private TextModel model;

    public MenuEightCommand(TextModel model) {
        this.model = model;
    }

    @Override
    public String execute() {
        StringBuilder sb = new StringBuilder();
        List<Word> allWords = model.getAllWords();
        List<Word> vowelWords = new ArrayList<>();
        for (int i = 0; i < allWords.size(); i++) {
            Pattern p = Pattern.compile(FIRST_VOWEL_PATTERN);
            Matcher m = p.matcher(allWords.get(i).getWord());
            if (m.find()) {
                vowelWords.add(allWords.get(i));
            }
        }
        Comparator<Word> comparator =
                (s1, s2) -> Character.compare(getNoneVowelChar(s1), getNoneVowelChar(s2));
        Collections.sort(vowelWords,comparator);
        vowelWords.forEach(e->sb.append("\n"+e.getWord()));
        return sb.toString();
    }

    private char getNoneVowelChar(Word word){
        int index = 0;
        Pattern p = Pattern.compile(FIRST_VOWEL_PATTERN);
        Matcher m = p.matcher(word.getWord().toLowerCase());
        while (m.find()) {
            index++;
        }
    if (index < word.getWord().length()) {
      return word.getWord().charAt(index);
        }
    else {
        return ' ';
    }
    }
}
