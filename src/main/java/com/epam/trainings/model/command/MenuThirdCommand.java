package com.epam.trainings.model.command;

import com.epam.trainings.model.command.Command;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;

import java.util.List;
import java.util.stream.Collectors;

public class MenuThirdCommand implements Command {
  private TextModel model;

  public MenuThirdCommand(TextModel model) {
    this.model = model;
  }

  @Override
  public String execute() {
      StringBuilder sb = new StringBuilder();
    List<Word> wordsInFirst = model.getSentences().get(0).getWordsInSentence();
    List<Sentence> sentencesList = model.getSentences();
    sentencesList.remove(0);
    List<Word> uniqueWords = wordsInFirst.stream().filter(e->{
        return sentencesList.stream().noneMatch(s->{
            return s.stringListOfWords().contains(e.getWord());
        });
    }).collect(Collectors.toList());
    uniqueWords.forEach(word->sb.append("\nWord : "+word.getWord()));
    return sb.toString();
  }
}
