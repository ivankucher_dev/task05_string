package com.epam.trainings.model.command;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.constants.RegexConst;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.TextModel;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuFirstCommand implements Command {

  private TextModel model;

  public MenuFirstCommand(TextModel model) {
    this.model = model;
  }

  @Override
  public String execute() {
    int count = 0;
    List<Sentence> sentences = this.model.getSentences();
    for (int i = 0; i < sentences.size(); i++) {
      Pattern pattern = Pattern.compile(RegexConst.DUPLICATE_WORD);
      Matcher matcher = pattern.matcher(sentences.get(i).toString());
      if (matcher.find()) {
        count++;
        continue;
      }
    }
    return CommandsConst.MENU_FIRST_TASK_RESULT_TEXT + " = "+ count;
  }
}
