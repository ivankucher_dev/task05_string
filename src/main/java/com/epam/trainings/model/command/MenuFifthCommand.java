package com.epam.trainings.model.command;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import static com.epam.trainings.constants.CommandsConst.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuFifthCommand implements Command {

  private TextModel model;

  public MenuFifthCommand(TextModel textModel) {
    this.model = textModel;
  }

  @Override
  public String execute() {
    StringBuilder sb = new StringBuilder();
    String longestWordInSent;
    Word wordToReplace;
    String s;
    for (int i = 0; i < model.getSentences().size(); i++) {
      Pattern p = Pattern.compile("(?i)\\b[aeiou][^\\s]*(?=(\\s|$))");
      Matcher m = p.matcher(model.getSentences().get(i).toString());
      if (m.find()) {
          wordToReplace = new Word(m.group());
        s = model.getSentences().get(i).toString();
        longestWordInSent = longestWordInSentence(s);
        int index = getIndexOfLongestWord(longestWordInSent,i);

          model.getSentences()
                  .get(i)
                  .getWordsInSentence()
                  .set(index,wordToReplace);

          s =  s.replaceAll(longestWordInSent,m.group());
          model.getSentences().get(i).refactorStringSentence(s);
          sb.append("\n["+longestWordInSent+"] replace to ["+m.group()+"]");
      }
    }
    model.getSentences().forEach(e -> sb.append("\n"+SENTENCE_NAME+ " : " +e.toString()));
    return sb.toString();
  }


  private String longestWordInSentence(String sentence){
     String longestWordInSent =
              Arrays.stream(sentence.split(" "))
                      .max(Comparator.comparingInt(String::length))
                      .orElse(null).replaceAll("[^a-zA-Z]", "").trim();

     return longestWordInSent;
  }

  private int getIndexOfLongestWord(String longWord , int sentenceNumber){
     List<Word> words = model.getSentences().get(sentenceNumber).getWordsInSentence();
      int index =-1;
    for (Word word : words) {
      if (word.getWord().equalsIgnoreCase(longWord)) {
        index = words.indexOf(word);
        break;
      }
          }
    return index;
  }
}
