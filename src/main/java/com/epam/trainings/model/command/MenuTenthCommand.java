package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.utils.MapUtil;

import java.util.*;
import java.util.stream.Collectors;

public class MenuTenthCommand implements Command {

  private TextModel model;
  private Map<String,Integer> wordCount;
  private Scanner sc;

  public MenuTenthCommand(TextModel model) {
    this.model = model;
    sc = new Scanner(System.in);
    wordCount = new LinkedHashMap<>();
  }

  @Override
  public String execute() {
      StringBuilder sb = new StringBuilder();
    List<Sentence> sentences = model.getSentences();
    List<String> words = new ArrayList<>();
      String word = " ";
    System.out.println("Enter words(enter 'stop' to stop) :");
    while (!word.equalsIgnoreCase("stop")) {
      word = sc.nextLine();
      if (!word.equals("stop")) {
        words.add(word);
      }
    }
    sentences.stream().map(Sentence::toString).forEach(sentence->{
        words.forEach(w->{
            if(sentence.contains(w)){
                if(wordCount.containsKey(w)){
                    wordCount.put(w,wordCount.get(w)+1);
                }
                else{
                    wordCount.put(w,1);
                }
            }
        });
    });
    wordCount = MapUtil.sortByValue(wordCount);
    wordCount.forEach((w,c)->sb.append("\n"+w+ " - "+c+ " times"));
    return sb.toString();
  }
}
