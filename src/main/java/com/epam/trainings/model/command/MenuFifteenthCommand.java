package com.epam.trainings.model.command;

import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MenuFifteenthCommand implements Command {

  private TextModel model;

  public MenuFifteenthCommand(TextModel model) {
    this.model = model;
  }

  @Override
  public String execute() {
    char firstSymb;
    StringBuilder sb = new StringBuilder();
    ArrayList<Character> wordSymbols;
    List<Word> allWords = model.getAllWords();
    for (Word word : allWords) {
        wordSymbols = new ArrayList<>();
      firstSymb = word.getWord().charAt(0);
      for (char c : word.getWord().toCharArray()) {
        wordSymbols.add(c);
      }
      for (int i = 1; i < wordSymbols.size(); i++) {
        if (firstSymb == wordSymbols.get(i)) {
         wordSymbols.remove(i);
        }
      }
      String w = wordSymbols.stream().map(String::valueOf).collect(Collectors.joining());
       sb.append("\n"+w);
      word.setWord(w);
    }

    return sb.toString();
  }
}
