package com.epam.trainings.model.command.executor;

import com.epam.trainings.model.command.Command;
import java.util.ArrayList;
import java.util.List;

public class CommandExecutor {

  private final List<Command> operations = new ArrayList<>();

  public String execute(Command command) {
    operations.add(command);
    return command.execute();
  }
}
