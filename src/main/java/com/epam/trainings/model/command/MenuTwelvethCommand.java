package com.epam.trainings.model.command;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuTwelvethCommand implements Command {

  private int length;
  private TextModel textModel;
  private Scanner sc;

  public MenuTwelvethCommand(TextModel textModel) {
    this.textModel = textModel;
    sc = new Scanner(System.in);
  }

  @Override
  public String execute() {
    StringBuilder sb = new StringBuilder();
    System.out.println("Enter length : ");
    length = sc.nextInt();
    List<Sentence> sentences = textModel.getSentences();
    List<Word> allWords = textModel.getAllWords();
    List<Word> wordsToRemove = new ArrayList<>();
    allWords.stream()
        .forEach(
            word -> {
              Pattern p = Pattern.compile(CommandsConst.FIRST_NO_VOWEL_PATTERN);
              Matcher m = p.matcher(word.getWord());
              if (word.getWord().length() == length && m.find()) {
                wordsToRemove.add(word);
              }
            });
    wordsToRemove.forEach(
        word -> {
          allWords.remove(word);
          sentences.stream()
              .forEach(
                  sentence -> {
                    String s = sentence.toString().replaceAll(word.getWord(), "");
                    sentence.refactorStringSentence(s);
                  });
        });
    sentences.stream()
        .map(Sentence::toString)
        .forEach(sentence -> sb.append("\nSentence : " + sentence));
    return sb.toString();
  }
}
