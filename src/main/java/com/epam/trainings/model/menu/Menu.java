package com.epam.trainings.model.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Menu implements SimpleMenu {
  private String greeting;
  private String languageChoose;
  private List<String> menu_tasks;
  private static Logger log = LogManager.getLogger(Menu.class.getName());

  public void setGreeting(String greeting) {
    this.greeting = greeting;
  }

  public void setLanguageChoose(String languageChoose) {
    this.languageChoose = languageChoose;
  }

  public void setNameForMenuTask(List<String> menuText) {
    menu_tasks = new ArrayList<>();
    menu_tasks = menuText;
  }

  @Override
  public String show() {
    log.info("Showing menu");
    StringBuilder sb = new StringBuilder();
    sb.append(languageChoose);
    menu_tasks.forEach(e -> sb.append("\n" + e));
    return sb.toString();
  }

  @Override
  public String greeting() {
    log.info("Greeting");
    StringBuilder sb = new StringBuilder();
    sb.append(greeting);
    return sb.toString();
  }
}
