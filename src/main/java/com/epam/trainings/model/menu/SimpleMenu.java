package com.epam.trainings.model.menu;

public interface SimpleMenu {
  String show();

  String greeting();
}
