package com.epam.trainings.model.textmodel.textmodelsubclasses;

public class Word {
  private String word;

  public Word(String word) {
    this.word = word;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }
}
