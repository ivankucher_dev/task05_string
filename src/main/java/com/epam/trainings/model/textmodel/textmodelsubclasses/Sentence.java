package com.epam.trainings.model.textmodel.textmodelsubclasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
  private String stringSentence;
  private List<Word> wordsInSentence;
  private List<Symbol> symbols;
  private boolean question;

  public Sentence(String stringSentence, boolean question) {
    this.stringSentence = stringSentence;
    wordsInSentence = new ArrayList<>();
    symbols = new ArrayList<>();
    this.question = question;
    initWordsBySentence();
    initSymbolsBySentence();
  }

  private void initWordsBySentence() {
    List<String> words = Arrays.asList(stringSentence.split("[\\p{IsPunctuation}\\s]+"));
    for (String word : words) {
      Word e = new Word(word);
      wordsInSentence.add(e);
    }
  }

  private void initSymbolsBySentence() {
    final Pattern pattern = Pattern.compile("\\p{IsPunctuation}");
    Matcher matcher = pattern.matcher(stringSentence);
    if (matcher.find()) {
      Symbol e = new Symbol(stringSentence.charAt(matcher.start()));
      symbols.add(e);
    }
  }

  public boolean isQuestion() {
    return question;
  }

  public List<Word> getWordsInSentence() {
    return wordsInSentence;
  }

  public List<String> stringListOfWords() {
    List<String> words = new ArrayList<>();
    wordsInSentence.forEach(word -> words.add(word.getWord()));
    return words;
  }

  public void refactorStringSentence(String newSentence) {
    this.stringSentence = newSentence;
  }

  public void setWordsInSentence(List<Word> wordsInSentence) {
    this.wordsInSentence = wordsInSentence;
  }

  public List<Symbol> getSymbols() {
    return symbols;
  }

  public void setSymbols(List<Symbol> symbols) {
    this.symbols = symbols;
  }

  @Override
  public String toString() {
    return stringSentence;
  }
}
