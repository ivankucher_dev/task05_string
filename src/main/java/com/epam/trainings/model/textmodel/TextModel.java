package com.epam.trainings.model.textmodel;

import com.epam.trainings.model.command.MenuFifthCommand;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Sentence;
import com.epam.trainings.model.textmodel.textmodelsubclasses.Word;

import static com.epam.trainings.constants.CommandsConst.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextModel {
  private URI path;
  private String text;
  private ArrayList<Sentence> sentences;

  public TextModel(URI pathToFile) {
    this.path = pathToFile;
    text = ReadStringFromFile.cleanAndReadText(path);
    sentences = new ArrayList<>();
    initSentencesByText(text);
  }

  private void initSentencesByText(String text) {
    List<String> stringSentences = new ArrayList<>(Arrays.asList(text.split("[.!?]\\s+")));
    List<String> subSentences = new ArrayList<>(Arrays.asList(text.split("[?]\\s*")));
    List<String> questionSentences = new ArrayList<>();
    for (String e : subSentences) {
      int length = e.split("[.!]\\s*").length;
      questionSentences.add(e.split("[.!]\\s*")[length - 1]);
    }
    for (String e : stringSentences) {
      Sentence s = new Sentence(e, true);
      sentences.add(s);
    }
  }

  public String getText() {
    return text;
  }

  public List<Word> getAllWords() {
    List<Word> allWords = new ArrayList<>();
    StringBuilder sb = new StringBuilder();
    sentences.forEach(
        sentence -> sentence.getWordsInSentence().forEach(word -> allWords.add(word)));
    return allWords;
  }

  private int splitLength(String[] arr) {
    return arr.length - 1;
  }

  public ArrayList<Sentence> getSentences() {
    return sentences;
  }
}
