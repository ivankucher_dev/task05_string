package com.epam.trainings.model;

import com.epam.trainings.constants.ConstSetuper;
import com.epam.trainings.constants.PresentLocale;
import com.epam.trainings.model.menu.Menu;
import java.util.*;
import static com.epam.trainings.constants.LanguageConst.*;

public class Language {
  private HashMap<Integer, String> avaliableLanguages;
  private List<String> menuText;
  private Menu menu;
  private ResourceBundle bundle;

  public Language(Menu menu) {
    this.menu = menu;
    avaliableLanguages = new HashMap<>();
    fillLanguagesMap();
    updateLanguage(new Locale(avaliableLanguages.get(1)));
    PresentLocale.setLocale(new Locale(avaliableLanguages.get(1)));
  }

  public HashMap<Integer, String> getAvaliableLanguages() {
    return avaliableLanguages;
  }

  private void fillLanguagesMap() {
    avaliableLanguages.put(1, EN);
    avaliableLanguages.put(2, UA);
    avaliableLanguages.put(3, CN);
  }

  public void showLanguageList() {
    avaliableLanguages
        .entrySet()
        .forEach(
            entry -> {
              System.out.println(entry.getKey() + " - " + entry.getValue());
            });
  }

  public void updateLanguage(Locale locale) {
    menuText = new ArrayList<>();
    PresentLocale.setLocale(locale);
    ConstSetuper.setup();
    updateMenuBasedOnLanguage(locale);
  }

  private void updateMenuBasedOnLanguage(Locale locale) {
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    menu.setGreeting(bundle.getString("greeting"));
    menu.setLanguageChoose(bundle.getString("languageChoose"));
    menuText.add(bundle.getString("menu_item1"));
    menuText.add(bundle.getString("menu_item2"));
    menuText.add(bundle.getString("menu_item3"));
    menuText.add(bundle.getString("menu_item4"));
    menuText.add(bundle.getString("menu_item5"));
    menuText.add(bundle.getString("menu_item6"));
    menuText.add(bundle.getString("menu_item7"));
    menuText.add(bundle.getString("menu_item8"));
    menuText.add(bundle.getString("menu_item9"));
    menuText.add(bundle.getString("menu_item10"));
    menuText.add(bundle.getString("menu_item11"));
    menuText.add(bundle.getString("menu_item12"));
    menuText.add(bundle.getString("menu_item13"));
    menuText.add(bundle.getString("menu_item14"));
    menuText.add(bundle.getString("menu_item15"));
    menuText.add(bundle.getString("menu_item16"));
    menuText.add(bundle.getString("menu_exit"));
    menu.setNameForMenuTask(menuText);
  }
}
