package com.epam.trainings.constants;

public class RegexConst {
  public static final String DUPLICATE_WORD = "\\b(\\w+)\\b\\s*(?=.*\\b\\1\\b)";
}
