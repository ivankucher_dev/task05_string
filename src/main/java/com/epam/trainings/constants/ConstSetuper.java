package com.epam.trainings.constants;

import java.util.ResourceBundle;

import static com.epam.trainings.constants.PresentLocale.locale;
import static com.epam.trainings.constants.CommandsConst.*;
import static com.epam.trainings.constants.LanguageConst.*;

public class ConstSetuper {
  public static ResourceBundle bundle = ResourceBundle.getBundle("MyMenu", locale);

  public static void setup() {
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    NO_COMMANDS = bundle.getString("NO_COMMANDS");
    LANGUAGE_CHANGED = bundle.getString("LANGUAGE_CHANGED");
    MENU_FIRST_TASK_RESULT_TEXT = bundle.getString("MENU_FIRST_TASK_RESULT_TEXT");
    SENTENCE_NAME = bundle.getString("SENTENCE_NAME");
  }
}
