package com.epam.trainings.constants;

public class LanguageConst {
  public static final String EN = "en";
  public static final String UA = "uk";
  public static final String CN = "cn";
  public static String LANGUAGE_CHANGED;
}
