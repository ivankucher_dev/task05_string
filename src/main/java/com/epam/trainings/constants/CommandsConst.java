package com.epam.trainings.constants;

public class CommandsConst {
  public static final char QUESTION_MARK = '?';
  public static final int COMMAND_CHANGE_LANGUAGE = 1;
  public static final int COMMAND_MENU_FIRST_TASK = 2;
  public static final int COMMAND_MENU_SECOND_TASK = 3;
  public static final int COMMAND_MENU_THIRD_TASK = 4;
  public static final int COMMAND_MENU_FOURTH_TASK = 5;
  public static final int COMMAND_MENU_FIFTH_TASK = 6;
  public static final int COMMAND_MENU_SIXTH_TASK = 7;
  public static final int COMMAND_MENU_SEVENTH_TASK = 8;
  public static final int COMMAND_MENU_EIGHT_TASK = 9;
  public static final int COMMAND_MENU_NINTH_TASK = 10;
  public static final int COMMAND_MENU_TENTH_TASK = 11;
  public static final int COMMAND_MENU_ELEVEN_TASK = 12;
  public static final int COMMAND_MENU_TVELVETH_TASK =13;
  public static final int COMMAND_MENU_THIRTEENTH_TASK = 14;
  public static final int COMMAND_MENU_FOURTEENTH_TASK = 15;
  public static final int COMMAND_MENU_FIFTEENTH_TASK = 16;
  public static final int COMMAND_MENU_SIXTEENTH_TASK = 17;
  public static final int COMMAND_MENU_EXIT =00;
  public static final String VOWEL_PATTERN_STR = "(?i)[aeiou]";
  public static final String FIRST_VOWEL_PATTERN = "(?i)\\b[aeiou][^\\s]*(?=(\\s|$))";
  public static final String FIRST_NO_VOWEL_PATTERN = "(?i)\\b[^aeiou][^\\s]*(?=(\\s|$))";
  public static String NO_COMMANDS;
  public static String MENU_FIRST_TASK_RESULT_TEXT;
  public static String SENTENCE_NAME;
}
