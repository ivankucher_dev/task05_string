package com.epam.trainings.constants;

import java.util.Locale;

public class PresentLocale {
  public static Locale locale;

  public static void setLocale(Locale locale) {
    PresentLocale.locale = locale;
  }
}
