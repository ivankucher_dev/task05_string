package com.epam.trainings.view;

import com.epam.trainings.constants.CommandsConst;
import com.epam.trainings.controller.ViewController;
import com.epam.trainings.exceptions.ControllerNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class View {

  private boolean firstStart;
  private ViewController controller;
  private static Logger log = LogManager.getLogger(View.class.getName());
  private Scanner scanner;

  public View() {
    firstStart = true;
    scanner = new Scanner(System.in);
  }

  public void modelChanged() {
    if (firstStart) firstStart = !firstStart;
    show();
  }

  public void setController(ViewController controller) {
    this.controller = controller;
    System.out.println(controller.greeting());
    show();
  }

  public void show() {
    if (controller == null) {
      try {
        throw new ControllerNotFoundException("set controller for view");
      } catch (ControllerNotFoundException e) {
        e.printStackTrace();
      }
    }
    if (firstStart) {
      controller.execute(CommandsConst.COMMAND_CHANGE_LANGUAGE);
    }
    String toShow = controller.showMenu();
    System.out.println(toShow);
    askForCommand();
  }

  public void show(String stringToShow) {
    log.info(stringToShow);
  }

  private void askForCommand() {
    log.info("Ask for command");
    int command;
    command = scanner.nextInt();
    controller.execute(command);
  }
}
