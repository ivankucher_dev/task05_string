package com.epam.trainings.controller;

import com.epam.trainings.model.Language;
import com.epam.trainings.model.command.*;
import com.epam.trainings.model.menu.Menu;
import static com.epam.trainings.constants.CommandsConst.*;
import static com.epam.trainings.constants.Const.*;

import com.epam.trainings.model.command.executor.CommandExecutor;
import com.epam.trainings.model.textmodel.TextModel;
import com.epam.trainings.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URISyntaxException;

public class ViewController implements Controller {

  private TextModel model;
  private View view;
  private Language language;
  private Menu menu;
  private CommandExecutor executor;
  private static Logger log = LogManager.getLogger(ViewController.class.getName());

  public ViewController(View view, Language language, Menu menu) {
    this.view = view;
    this.language = language;
    this.menu = menu;
    executor = new CommandExecutor();
    readFileToString();
  }

  @Override
  public String execute(int command) {
    String result = NO_COMMANDS;
    switch (command) {
      case COMMAND_CHANGE_LANGUAGE:
        result = executor.execute(new ChangeLanguageCommand(language));
        break;

      case COMMAND_MENU_FIRST_TASK:
        result = executor.execute(new MenuFirstCommand(model));
        break;

      case COMMAND_MENU_SECOND_TASK:
        result = executor.execute(new MenuSecondCommand(model));
        break;

      case COMMAND_MENU_THIRD_TASK:
        result = executor.execute(new MenuThirdCommand(model));
        break;

      case COMMAND_MENU_FOURTH_TASK:
        result = executor.execute(new MenuFourthCommand(model, 1));
        break;

      case COMMAND_MENU_FIFTH_TASK:
        result = executor.execute(new MenuFifthCommand(model));
        break;

      case COMMAND_MENU_SIXTH_TASK:
        result = executor.execute(new MenuSixthCommand(model));
        break;

      case COMMAND_MENU_SEVENTH_TASK:
        result = executor.execute(new MenuSeventhCommand(model));
        break;

      case COMMAND_MENU_EIGHT_TASK:
        result = executor.execute(new MenuEightCommand(model));
        break;

      case COMMAND_MENU_NINTH_TASK:
        result = executor.execute(new MenuNinthCommand(model));
        break;

      case COMMAND_MENU_TENTH_TASK:
        result = executor.execute(new MenuTenthCommand(model));
        break;

      case COMMAND_MENU_ELEVEN_TASK:
        result = executor.execute(new MenuTenthCommand(model));
        break;

      case COMMAND_MENU_TVELVETH_TASK:
        result = executor.execute(new MenuTwelvethCommand(model));
        break;

      case COMMAND_MENU_THIRTEENTH_TASK:
        result = executor.execute(new MenuThirteenthCommand(model));
        break;

      case COMMAND_MENU_FOURTEENTH_TASK:
        result = executor.execute(new MenuFourteenthCommand(model));
        break;

      case COMMAND_MENU_FIFTEENTH_TASK:
        result = executor.execute(new MenuFifteenthCommand(model));
        break;

      case COMMAND_MENU_SIXTEENTH_TASK:
        result = executor.execute(new MenuSixteenthCommand(model));
        break;

      case COMMAND_MENU_EXIT:
        System.exit(1);
    }
    if (result.equals(NO_COMMANDS)) {
      log.error(NO_COMMANDS);
    }
    view.show(result);
    view.modelChanged();
    return result;
  }

  @Override
  public void readFileToString() {
    try {
      model = new TextModel(getClass().getResource(DEFAULT_PATH).toURI());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  public String showMenu() {
    return menu.show();
  }

  public String greeting() {
    return menu.greeting();
  }
}
