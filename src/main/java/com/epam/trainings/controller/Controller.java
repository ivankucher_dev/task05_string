package com.epam.trainings.controller;

public interface Controller {
  String execute(int command);

  String greeting();

  void readFileToString();
}
