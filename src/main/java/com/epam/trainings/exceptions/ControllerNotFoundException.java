package com.epam.trainings.exceptions;

public class ControllerNotFoundException extends Exception {
  public ControllerNotFoundException(String message) {
    super(message);
  }
}
